package sk.fiit.pdt.models;

/**
 * Created by virtual on 10.11.2015.
 */
public class Query {

    private String query;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
