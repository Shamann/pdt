package sk.fiit.pdt.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by virtual on 10.11.2015.
 */
public class DBConnection {

    private static DBConnection singleton = new DBConnection();
    private Connection connection;


    private DBConnection(){
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/pdt","pdt", "pdt");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static DBConnection getConnection(){
        return singleton;
    }

    public List<String> executeQuery(String query){
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            List<String> results = new ArrayList<>();
            while (resultSet.next())
            {
                System.out.println(resultSet.getString(1));
                results.add(resultSet.getString(resultSet.findColumn("name_2")));
            }
            resultSet.close();
            statement.close();
            return results;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void closeConnection(){
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
