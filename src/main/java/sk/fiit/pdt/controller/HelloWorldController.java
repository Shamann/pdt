package sk.fiit.pdt.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;
import sk.fiit.pdt.db.DBConnection;
import sk.fiit.pdt.models.Query;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by janko on 21.9.2015.
 */
public class HelloWorldController extends AbstractController {

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        ModelAndView model = new ModelAndView("HelloWorldPage","command", new Query());


        String query = httpServletRequest.getParameter("query");
        if(query!=null){
            List<String> results = DBConnection.getConnection().executeQuery(query); //"SELECT * FROM svk_adm2 LIMIT 10"
            model.addObject("msg", StringUtils.join(results,"<br>"));
        } else {
            model.addObject("msg", "Write query and press submit");
        }
        model.addObject("json", getGeojson());

        return model;
    }

    private String getGeojson(){
        return "    {\n" +
                "        \"type\": \"FeatureCollection\",\n" +
                "        \"features\": [{\n" +
                "            \"type\": \"Feature\",\n" +
                "            \"properties\": {\n" +
                "                \"fillColor\": \"#eeffee\",\n" +
                "                \"fillOpacity\": 0.8\n" +
                "            },\n" +
                "            \"geometry\": {\n" +
                "                \"type\": \"Polygon\",\n" +
                "                \"coordinates\": [\n" +
                "                    [\n" +
                "                        [119.2895599, 21.718679],\n" +
                "                        [119.2895599, 25.373809],\n" +
                "                        [122.61840, 25.37380917],\n" +
                "                        [122.61840, 21.71867980],\n" +
                "                        [119.2895599, 21.718679]\n" +
                "                    ]\n" +
                "                ]\n" +
                "            }\n" +
                "        }, {\n" +
                "            \"type\": \"Feature\",\n" +
                "            \"properties\": {\n" +
                "                \"marker-color\": \"#00ff00\"\n" +
                "            },\n" +
                "            \"geometry\": {\n" +
                "                \"type\": \"Point\",\n" +
                "                \"coordinates\": [120.89355, 23.68477]\n" +
                "            }\n" +
                "        }]\n" +
                "    };";
    }

//    protected JSON
}
