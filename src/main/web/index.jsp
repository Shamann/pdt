<%--
  Created by IntelliJ IDEA.
  User: janko
  Date: 21.9.2015
  Time: 23:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset=utf-8 />
    <title>A load GeoJson polygon</title>
    <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />
    <script src='https://api.mapbox.com/mapbox.js/v2.2.3/mapbox.js'></script>
    <script src='js/accessToken.js'></script>
    <link href='https://api.mapbox.com/mapbox.js/v2.2.3/mapbox.css' rel='stylesheet' />
    <style>
        body { margin:0; padding:0; }
        #map { position:absolute; top:100pt; bottom:0; width:100%; }
    </style>
</head>
  <body>
    <h1>Working</h1>

    <div id='text'>Hello</div>


    <div id='map'></div>
    <script>
        L.mapbox.accessToken = accessToken;
        var map = L.mapbox.map('map', 'mapbox.streets')
                .setView([23.68477, 120.89355], 7);

        // GeoJSON data: see http://geojson.org/ for the full description of this format.
        //
        // This specific GeoJSON document includes Path styling that's based on Leaflet's
        // convention: http://leafletjs.com/reference.html#path
        //
        // And marker styling based on simplestyle-spec:
        // http://github.com/mapbox/simplestyle-spec
        var geoJsonData = {
            "type": "FeatureCollection",
            "features": [{
                "type": "Feature",
                "properties": {
                    "fillColor": "#eeffee",
                    "fillOpacity": 0.8
                },
                "geometry": {
                    "type": "Polygon",
                    "coordinates": [
                        [
                            [119.2895599, 21.718679],
                            [119.2895599, 25.373809],
                            [122.61840, 25.37380917],
                            [122.61840, 21.71867980],
                            [119.2895599, 21.718679]
                        ]
                    ]
                }
            }, {
                "type": "Feature",
                "properties": {
                    "marker-color": "#00ff00"
                },
                "geometry": {
                    "type": "Point",
                    "coordinates": [120.89355, 23.68477]
                }
            }]
        };

        // Here we use the `L.geoJson` layer type from Leaflet, that lets us use
        // Polygons, Points, and all other GeoJSON types - but we specify that it
        // should also pull styles for polygons with the `style` option
        // and use the custom `L.mapbox.marker.style` function
        // to make fancy markers with the `pointToLayer` option.
        var geoJson = L.geoJson(geoJsonData, {
            pointToLayer: L.mapbox.marker.style,
            style: function(feature) { return feature.properties; }
        }).addTo(map);
    </script>



  </body>
</html>